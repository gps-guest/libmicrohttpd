Source: libmicrohttpd
Section: libs
Priority: optional
Maintainer: Bertrand Marc <bmarc@debian.org>
Build-Depends: debhelper (>= 9), autoconf, automake, dh-autoreconf,
 libcurl4-gnutls-dev, libgnutls28-dev, texinfo, pkg-config, socat[!hurd-i386],
 zzuf
Standards-Version: 4.4.0
Vcs-Git: https://salsa.debian.org/debian/libmicrohttpd.git
Vcs-browser: https://salsa.debian.org/debian/libmicrohttpd
Homepage: http://www.gnu.org/software/libmicrohttpd/

Package: libmicrohttpd12
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library embedding HTTP server functionality
 GNU libmicrohttpd is a small C library that is supposed to make it easy to run
 an HTTP server as part of another application. Key features that distinguish
 GNU Libmicrohttpd from other projects are:
  * C library: fast and small
  * API is simple, expressive and fully reentrant
  * Implementation is HTTP 1.1 compliant
  * HTTP server can listen on multiple ports
  * Four different threading models (select, poll, pthread, thread pool)
  * Support for IPv6
  * Support for SHOUTcast
  * Support for incremental processing of POST data (optional)
  * Support for basic and digest authentication (optional)
  * Support for SSL3 and TLS

Package: libmicrohttpd-dev
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends}, libmicrohttpd12 (= ${binary:Version}), libgnutls28-dev,
 libgcrypt-dev
Description: library embedding HTTP server functionality (development)
 GNU libmicrohttpd is a small C library that is supposed to make it easy to run
 an HTTP server as part of another application. Key features that distinguish
 GNU Libmicrohttpd from other projects are:
  * C library: fast and small
  * API is simple, expressive and fully reentrant
  * Implementation is HTTP 1.1 compliant
  * HTTP server can listen on multiple ports
  * Four different threading models (select, poll, pthread, thread pool)
  * Support for IPv6
  * Support for SHOUTcast
  * Support for incremental processing of POST data (optional)
  * Support for basic and digest authentication (optional)
  * Support for SSL3 and TLS
 .
 This package contains the development files.
